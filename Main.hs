module Main
where

import Control.Monad (when, forever)
import System.IO
import System.Exit
import System.Environment
import System.ZMQ
import qualified Data.ByteString.Char8 as B
import Data.Char (isSpace)

barf :: String -> IO ()
barf msg = do
    hPutStrLn stderr msg
    hFlush stderr
    exitFailure

main :: IO ()
main = do
    args <- getArgs
    print args
    when (length args < 1) $ barf "Need at least one argument"
    let modeS = head args
    case modeS of
        "listen" -> readerMain
        "speak" -> writerMain
        m -> barf $ "Unknown operation mode '" ++ m ++ "'"
    

readerMain =
    withContext 1 $ \context ->
    withSocket context Pull $ \receiver -> do
        bind receiver "tcp://*:5555"
        hPutStrLn stderr "Listening on port 5555"

        forever $ do
            ln <- receive receiver []
            B.putStrLn $
                fst $ B.spanEnd isSpace ln
            hFlush stdout

writerMain =
    withContext 1 $ \context ->
    withSocket context Push $ \sender -> do
        connect sender "tcp://*:5555"
        go sender
        where
            go sender =
                isEOF >>= \eof ->
                if eof
                    then exitSuccess
                    else do
                        msg <- B.getLine
                        send sender msg []
                        go sender
